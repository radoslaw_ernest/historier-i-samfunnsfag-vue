import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Chapter from './views/Chapter.vue';
import Content from './views/Content.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/:lang',
      name: 'home',
      props: true,
      component: Home
    },
    {
      path: '/:lang/:chapter/:chapterName',
      name: 'chapter',
      props: true,
      component: Chapter
    },
    {
      path: '/:lang/:chapter/:chapterName/Laererfortelling',
      name: 'content',
      props: (route) => ({ 
        lang: route.params.lang,
        chapter: route.params.chapter,
        contentType: 'story'
      }),
      component: Content
    },
    {
      path: '/:lang/:chapter/:chapterName/Oppgaver-og-aktiviteter',
      name: 'content',
      props: (route) => ({ 
        lang: route.params.lang,
        chapter: route.params.chapter,
        contentType: 'exercises'
      }),
      component: Content
    },
  ]
});
